import React, { Component } from 'react';

export default class Task extends Component {
  render() {
    return (
      <div>
        <div className="container-fluid">
          <div className="phone">
            <div className="camera" />
            <div className="camera_lens" />
            <div className="mark" />
            <div className="speaker" />
            <div className="screen">
              <div className="topbar">
                <i className="fa fa-wifi" />

                <span>11:41</span>
              </div>
              <div className="calculator " id="calculator">
                <div className="row p-2">
                  <div className="col">
                    <div
                      style={{ backgroundColor: '#0b9dcf', height: '50px' }}
                      className="card p-2  "
                    >
                      <h6>ALL CANDIDATES</h6>
                    </div>

                    <div className="card mt-5 p-2 ">
                      <div className="row">
                        <div className="col-4 text-dark ">
                          <img
                            className="img-fluid "
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4u-bl-y3cRViZ4FRx5vxDCSO4dyr1GM7-RctGJkpw5x_j8B4G"
                            class="rounded-circle"
                            alt="Cinque Terre"
                            width="44"
                            height="43"
                          />
                          <p
                            className="mt-2 text-center"
                            style={{ fontSize: '14px', color: '#26a595' }}
                          >
                            96%
                          </p>

                          <p
                            className="text-center"
                            style={{
                              color: '#a1a1a1',
                              fontSize: '8px',
                              marginTop: '-20px'
                            }}
                          >
                            Matched
                          </p>
                          <div
                            class="form-check text-center"
                            style={{ marginTop: '-10px' }}
                          >
                            <input
                              class="form-check-input position-static"
                              type="checkbox"
                              id="blankCheckbox"
                              value="option1"
                              aria-label="..."
                            />
                          </div>
                        </div>
                        <div className="col-8    ">
                          <h4
                            style={{
                              color: '#0b9dcf',
                              fontSize: '18px'
                            }}
                          >
                            Ruby
                          </h4>

                          <p
                            style={{
                              fontSize: '10px',
                              color: '#a1a1a1'
                            }}
                          >
                            Noida,India
                            <br />
                            Phone:
                            <span style={{ fontWeight: 'bold' }}>
                              {' '}
                              +1732000444
                            </span>
                            <br />
                            Email:
                            <span style={{ fontWeight: 'bold' }}>
                              {' '}
                              ruby20@gmail.com
                            </span>
                            <br />
                            Applied:
                            <span style={{ fontWeight: 'bold' }}>
                              {' '}
                              Software Developer
                            </span>
                          </p>
                        </div>
                      </div>
                      <div className="row p-1 text-dark ">
                        <div className="col-4 ">
                          <span style={{ fontSize: '12px' }}> Staus</span>
                        </div>
                        <div className="col-5">
                          <div>
                            <span
                              style={{
                                color: '#0b9ed0',

                                fontSize: '11px'
                              }}
                            >
                              {' '}
                              SHORTLIST
                            </span>
                          </div>
                        </div>
                        <div className="col-3 ">
                          <span style={{ fontSize: '12px' }}> Fail</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="lock" />
            <div className="volume" />
          </div>
        </div>
      </div>
    );
  }
}
